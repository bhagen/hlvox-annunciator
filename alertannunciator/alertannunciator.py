import os
import sys
import logging
import argparse
import pprint
import time
from pathlib import Path
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ReplyKeyboardMarkup
import pychromecast as pyc
import re
import requests
import json


class AlertAnnunciator():
    def __init__(self, config_path: str):
        self.config = self.read_config(Path(config_path))

        self.mc, self.cast = self.get_chromecast(
            self.config['default_chromecast'])

        self.voice = self.config['default_voice']
        self.token = self.config['bot_token']
        self.trigger = self.config['trigger']
        self.phrases = self.config['phrases']
        self.api_url = self.config['api_url']

        self.start()

    def read_config(self, path: Path) -> str:
        if not path.exists():
            logging.info(f"Config at {path.resolve()} does not exist.")
            sys.exit()

        with open(path, 'r') as f:
            return json.load(f)

    # See if the trigger word is in the message, then play a corresponding vox message
    def process_message(self, update, context):
        to_say = None
        specified_voice = None

        if update.channel_post:
            message = update.channel_post.text
            logging.info(f"Message posted in{update.channel_post.chat.title}")

            if self.trigger in message:
                logging.info("annunciator triggered")
                keyword = re.search('\{(.*)\}', message).group(1)

                to_say = self.phrases[keyword]
            else:
                logging.info("no keyword, ignoring message")
                return False

        elif update.message:
            message = update.message.text
            logging.info(f"direct message received: {message}")

            if all(char in message for char in ['[', ']']):
                logging.debug("Message contains voice specifier")

                try:
                    requested_voice = re.search('\[(.*)\]', message).group(1)
                except:
                    logging.error(
                        f"Badly formatted voice specifier: {message}")
                    return False

                r = requests.get(f"{self.api_url}/api/voices")
                r_content = json.loads(r.content)

                if requested_voice in r_content['voices']:
                    specified_voice = requested_voice
                else:
                    logging.warning(
                        f"Requested voice {requested_voice} not available")
                    specified_voice = None

                message = message.replace(f"[{specified_voice}]", '')

            to_say = message
        else:
            logging.error("unknown message type or source received")
            return False

        if to_say:
            logging.info(f"asking to say {to_say}", )

            voice = specified_voice if specified_voice is not None else self.voice
            self.play_message(to_say, voice, update)
            return True

    def play_message(self, sentence: str, voice: str, update):
        r = requests.get(
            f"{self.api_url}/api/voice/{voice}/generate", params={"sentence": sentence})

        r_content = json.loads(r.content)

        logging.debug(f"Content: {r_content}")

        if update.message is None:
            message = update.effective_message
        else:
            message = update.message

        if "status" in r_content.keys():
            message.reply_text(
                f"Error in REST response. Status: {r_content['status']}, Details: {r_content['details']}")
        else:
            # If there are no words to say, don't.
            if len(r_content["sayable"]) == 0:
                logging.error(
                    f"No words in {r_content['sentence']} could be said")

                message.reply_text("Can't say any of that")
            else:
                if len(r_content["unsayable"]) != 0:
                    logging.warning(
                        f"Couldn't say {r_content['unsayable']} but continuing with sayable words")
                audiopath = self.api_url + r_content['path']

                # Send sentence info to chat
                message.reply_text(
                    f"Sayable: {r_content['sayable']}, Unsayable: {r_content['unsayable']}")

                # TODO: More research on what these actually do. What happens if something is already playing?
                self.mc.play_media(audiopath, 'audio/wav')
                # self.mc.block_until_active()
                # self.mc.pause()

    def chromecast_setup(self, update, context):
        avail_ccs = pyc.get_chromecasts()
        avail_ccs_names = [cc.device.friendly_name for cc in avail_ccs]

        if len(avail_ccs_names) == 0:
            update.message.reply_text("No Chromecasts found")
        else:
            keyboard = [[InlineKeyboardButton(cc_name, callback_data="cc_" + cc_name)]
                        for cc_name in avail_ccs_names]

            reply_markup = InlineKeyboardMarkup(keyboard)

            update.message.reply_text(
                'Select a Chromecast device to play vox through:', reply_markup=reply_markup)

    def voice_setup(self, update, context):
        r = requests.get(f"{self.api_url}/api/voices")
        r_content = json.loads(r.content)

        keyboard = [[InlineKeyboardButton(
            avail_voice, callback_data="voice_" + avail_voice)] for avail_voice in r_content["voices"]]

        reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(
            'Select a voice:', reply_markup=reply_markup)

    def button(self, update, context):
        query = update.callback_query

        if "voice_" in query.data:
            self.voice = query.data.replace("voice_", "")
            logging.info(f"Setting voice to {self.voice}")

            query.edit_message_text(text=f"Selected Voice: {self.voice}")

        if "cc_" in query.data:
            cc_name = query.data.replace("cc_", "")

            self.cast, self.mc = self.get_chromecast(cc_name)

            query.edit_message_text(text=f"Selected Chromecast: {cc_name}")

    def get_chromecast(self, cc_name: str):
        logging.info(f"Setting CC to {cc_name}")

        avail_ccs = pyc.get_chromecasts()
        avail_ccs_names = [cc.name for cc in avail_ccs]

        try:
            cast = next(cc for cc in avail_ccs
                        if cc.name == cc_name)
        except StopIteration:
            logging.error(
                f"Chromecast {cc_name} not found. Available chromecasts: {avail_ccs_names}")
            sys.exit()

        cast.wait()
        mc = cast.media_controller
        logging.info(f"Found Chromecast '{cc_name}'")

        return cast, mc

    def error(self, update, context):
        logging.warning(f"Update {update} caused error {context.error}")

    def start(self):
        updater = Updater(
            self.token, use_context=True)

        dp = updater.dispatcher

        # Normal message handler
        dp.add_handler(MessageHandler(Filters.text, self.process_message))

        # Chromecast configuration
        dp.add_handler(CommandHandler("chromecast", self.chromecast_setup))
        dp.add_handler(CommandHandler("voice", self.voice_setup))
        dp.add_handler(CallbackQueryHandler(self.button))

        dp.add_error_handler(self.error)

        updater.start_polling()

        updater.idle()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='HLVox Alert Annunciator')
    parser.add_argument('config', type=str)

    args = parser.parse_args()

    aa = AlertAnnunciator(args.config)
