# HLVox Annunciator
Uses a Telegram bot to play sentences from a [hlvox-website](https://gitlab.com/bhagen/hlvox-website) over a local chromecast.

## Configuration
* Create a bot and get it's token using [BotFather](https://core.telegram.org/bots#creating-a-new-bot)
* Set the token and other defaults in `config.yaml`

## Use
### Channels:
The `phrases` section of the config file sets what the annunciator will play if it sees the trigger word in a channel that it is a part of. For example, if the trigger is set to `annunciate` and this phrase is in the `phrases` portion of the config: `{"greeting": "hello there"}`, then the annunciator will play "hello there" over the chromecast if it sees a message containing "annunciate greeting".

### Direct Messages:
There are two commands available: `/voice` to set the default voice and `/chromecast` to change which cast device the vox will speak on.
The annunciator will attempt to say any sentence that is sent to it as a direct message with the default voice on the selected chromecast.
You can use `[]` to specify a voice to be used for just one message. For example, `[hl1] hello` will be said with the hl1 voice, even if the default voice is something else. This will not set the default voice to hl1.

## Installation
`pip install -r requirements.txt`

## Running
The only arg needed is a path to the config file
`python alertannunciator/alertannunciator.py ./config.json`
